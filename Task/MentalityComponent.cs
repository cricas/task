﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class MentalityComponent : AbstractComponent
    {
        private readonly Mentality mentality;

        public MentalityComponent(IEntity entity, Mentality mentality) : base(entity) {
            this.mentality = mentality;
        }

        public Mentality GetMentality() => this.mentality;

        public override void Post(IEvent eventPost) { }
    }
}
