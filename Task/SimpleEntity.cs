﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class SimpleEntity : Entity
    {
        public SimpleEntity() {
            AttachComponent(new CollisionComponent(this))
                .AttachComponent(new LifeComponent(this));
        }
    }
}
