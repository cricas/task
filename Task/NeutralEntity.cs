﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class NeutralEntity : SimpleEntity
    {
        public NeutralEntity() => AttachComponent(new MentalityComponent(this, Mentality.Neutral));
    }
}
