﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class Simple
    {
        public double X { get; set; }
        public List<int> List { get; set; }

        public Simple()
        {
            X = 3.0;
            List = new List<int>
            {
                5,
                3,
                1,
                6,
                9,
                5
            };
        }
    }
}
