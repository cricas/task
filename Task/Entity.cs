﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task
{
    public class Entity : IEntity
    {
        readonly List<IComponent> Components;
        public double Damage { get; set; }

        public Entity()
        {
            Damage = 1;
            Components = new List<IComponent>();
        }
        public IEntity AttachComponent(IComponent c)
        {
            Components.Add(c);
            return this;
        }

        public void DetachComponent(Type componentType)
        {
            try
            {
                IComponent aux = Components
                                      .Where(c => componentType.IsInstanceOfType(c))
                                      .First();
                Components.Remove(aux);
            }
            catch
            {
                Console.WriteLine("component not found");
            }
        }

        public T GetComponent<T>() where T : IComponent
        {
            T aux;
            try
            {
                aux = (T)Components
                        .Where(c => c is T)
                        .First();
            }
            catch
            {
                Console.WriteLine("component not found");
                return default;
            }
            return aux;
        }

        public void Post(IEvent eventPost)
        {
            Components.ForEach(c => c.Post(eventPost));
        }
    }
}
