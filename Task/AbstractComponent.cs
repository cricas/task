﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public abstract class AbstractComponent : IComponent
    {
        private readonly IEntity entity;

        public AbstractComponent(IEntity entity)
        {
            this.entity = entity;
        }

        public IEntity GetEntity() 
        {
            return this.entity;
        }

        public abstract void Post(IEvent eventPost);
    }
}
