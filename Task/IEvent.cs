﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public interface IEvent
    {
        IEntity GetSourceEntity();
    }
}
