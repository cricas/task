﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class GoodEntity : SimpleEntity
    {
        public GoodEntity() => AttachComponent(new MentalityComponent(this, Mentality.Good));
    }
}
