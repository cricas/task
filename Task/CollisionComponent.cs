﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class CollisionComponent : AbstractComponent
    {
        public CollisionComponent(IEntity entity) : base(entity) { }
        public override void Post(IEvent eventPost)
        {
            if (eventPost is CollisionEvent)
            {
                MentalityComponent mentalitySource = eventPost.GetSourceEntity().GetComponent<MentalityComponent>();
                MentalityComponent mentalityMy = this.GetEntity().GetComponent<MentalityComponent>();
                Mentality source = mentalitySource == null ? Mentality.Neutral : mentalitySource.GetMentality();
                Mentality my = mentalityMy == null ? Mentality.Neutral : mentalityMy.GetMentality();

                if (
                    (my == Mentality.Evil && source == Mentality.Good)
                    || (my == Mentality.Good && source == Mentality.Evil)
                    || (my != Mentality.Neutral && source == Mentality.Psycho)
                   )
                {
                    GetEntity().Post(new DamageEvent(eventPost.GetSourceEntity()));
                }
            } 
        }
    }
}
