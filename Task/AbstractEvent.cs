﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public abstract class AbstractEvent : IEvent
    {
        private readonly IEntity entity;

        public AbstractEvent(IEntity entity)
        {
            this.entity = entity;
        }
        public IEntity GetSourceEntity()
        {
            return this.entity;
        }
    }
}
