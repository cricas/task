﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class PsychoEntity : SimpleEntity 
    {
        public PsychoEntity() => AttachComponent(new MentalityComponent(this, Mentality.Psycho));
    }
}
