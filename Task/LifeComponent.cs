﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class LifeComponent : AbstractComponent
    {
        public double Life { get; set; }
        public LifeComponent(IEntity entity) : base(entity)
        {
            Life = 10.0;
        }

        public override void Post(IEvent eventPost)
        {
            if(eventPost is DamageEvent)
            {
                Life -= eventPost.GetSourceEntity().Damage;
            }
        }
    }
}
