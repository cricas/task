﻿namespace Task
{
    public interface IComponent
    {
        void Post(IEvent eventPost);
    }
}