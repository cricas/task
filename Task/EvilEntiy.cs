﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public class EvilEntiy : SimpleEntity
    {
        public EvilEntiy() => AttachComponent(new MentalityComponent(this, Mentality.Evil));
    }
}
