﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task
{
    public interface IEntity
    {
        double Damage { get; set; }
        IEntity AttachComponent(IComponent c);
        void DetachComponent(Type componentType);
        T GetComponent<T>() where T : IComponent;
        void Post(IEvent eventPost);
    }
}
