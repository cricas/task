using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using Task;

namespace Test
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestCSharp()
        {
            Assert.AreEqual(3, 3);
            Simple simple = new Simple();
            Assert.AreEqual(simple.X, 3);

            //test vector and stream
            CollectionAssert.AreEqual(new List<int> { 5, 3, 1, 6, 9, 5 }, simple.List);
            List<int> stream = simple.List.Where(x => x > 5).ToList();
            CollectionAssert.AreEqual(new List<int> { 6, 9 }, stream);
            stream = (from x in simple.List
                               where x > 5
                               orderby x
                               select x).ToList();
            CollectionAssert.AreEqual(new List<int> { 6, 9 }, stream);
        }

        [TestMethod]
        public void TestSearchComponent() {
            IEntity Player = new GoodEntity();
            Assert.IsNotNull(Player.GetComponent<MentalityComponent>());
            Assert.IsNotNull(Player.GetComponent<CollisionComponent>());
            Assert.IsNotNull(Player.GetComponent<LifeComponent>());
            Assert.AreEqual(Player.GetComponent<MentalityComponent>().GetMentality() , Mentality.Good);
        }
        [TestMethod]
        public void TestComponents(){
            IEntity Player = new GoodEntity();
            IEntity Palyer2 = new GoodEntity();
            IEntity Enemy = new EvilEntiy();
            IEntity Stone = new NeutralEntity();
            IEntity Fire = new PsychoEntity();

            Assert.AreEqual(10.0, Player.GetComponent<LifeComponent>().Life);
            Assert.AreEqual(1, Enemy.Damage);
            double life = 10;
            double damge = 1;

            Player.Post(new CollisionEvent(Enemy));
            Assert.AreEqual(life - damge, Player.GetComponent<LifeComponent>().Life);

            Player.Post(new CollisionEvent(Palyer2));
            Assert.AreEqual(life - damge, Player.GetComponent<LifeComponent>().Life);

            Player.Post(new CollisionEvent(Stone));
            Assert.AreEqual(life - damge, Player.GetComponent<LifeComponent>().Life);

            Player.Post(new CollisionEvent(Fire));
            Assert.AreEqual(life - (damge + damge) , Player.GetComponent<LifeComponent>().Life);

        }
    }
}
